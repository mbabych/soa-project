### What is this repository for? ###

* This is a project in which JSON passes from JSP page to the ActiveMQ server in the queue and later get consumed by listener and persisted in MongoDB. It is implemented using Spring Framework. Project is tested using JUnit tests.