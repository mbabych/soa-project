package com.nikita.jms.producer.model;

import org.springframework.stereotype.Component;

@Component
public class Vendor {
	private String vendorName;
	
	private String firstName;
	private String lastName;
	private String address;
	private String city;
	
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		if(null == vendorName || vendorName.isEmpty()){
			vendorName = "vendorName";
		}
		this.vendorName = vendorName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		if(null == firstName || firstName.isEmpty()){
			firstName = "firstName";
		}
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		if(null == lastName || lastName.isEmpty()){
			lastName = "lastName";
		}
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		if(null == address || address.isEmpty()){
			address = "address";
		}
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		if(null == city || city.isEmpty()){
			city = "city";
		}
		this.city = city;
	}
	@Override
	public String toString() {
		return "Vendor [vendorName=" + getVendorName() + ", firstName=" + getFirstName() + ", lastName=" + getLastName() + ", address="
				+ getAddress() + ", city=" + getCity();
	}

}