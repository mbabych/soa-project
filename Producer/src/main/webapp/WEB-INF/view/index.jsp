<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta charset="utf-8">
<title>Vendor Entry</title>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/css/style.css'/>" />
</head>


<body>
<div>
<img alt="Vendor Entry" src=" <c:url value='/images/logo.png'/>"/>
</div>
	<div>
		<h2>Vendor Entry</h2>
	</div>
	<div id="form">
		<form:form modelAttribute="vendor" action="vendor">
		<div class="message">
		<c:if test="${!empty message}"></c:if>
		<c:out value="${message}"></c:out>
		</div>

			<fieldset>
				<legend>Vendor Information</legend>
				<div>
					<label for="vendorName">Vendor Name</label>
					<form:input path="vendorName" />
				</div>
				<div>
					<label for="firstName">First Name</label>
					<form:input path="firstName" />
				</div>
				<div>
					<label for="lastName">Last Name</label>
					<form:input path="lastName" />
				</div>
				<div>
					<label for="address">Address</label>
					<form:input path="address" />
				</div>

				<div>
					<label for="city">City</label>
					<form:select path="city">
						<form:option value="Kyiv">Kyiv</form:option>
						<form:option value="Sumy">Sumy</form:option>
						<form:option value="Kharkiv">Kharkiv</form:option>
					</form:select>
				</div>
				<div>
					<input type="submit" value="Submit" />
				</div>
			</fieldset>
		</form:form>
	</div>
</body>
</html>
