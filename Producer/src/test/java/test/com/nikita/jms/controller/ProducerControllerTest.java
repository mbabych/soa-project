package test.com.nikita.jms.controller;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import com.nikita.jms.producer.controller.ProducerController;
import com.nikita.jms.producer.model.Vendor;

public class ProducerControllerTest {
	private Vendor vendor;
	private Model model;
	private ProducerController producerController;
	private ApplicationContext context;
	
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		producerController = (ProducerController) context.getBean("producerController");
		vendor = new Vendor();
		vendor.setVendorName("Microsoft");
		vendor.setFirstName("Nikita");
		vendor.setLastName("Babych");
		vendor.setAddress("Lomonosova, 35");
		vendor.setCity("Kyiv");	
	}

	@After
	public void tearDown() throws Exception {
		
	}
	@Test
	public void testRenderVendorPage() {
		assertEquals("index", producerController.renderVendorPage(vendor, model));
	}	
	@Test
	public void testProcessRequest() {
		ModelAndView mv = producerController.processRequest(vendor, model);
		assertEquals("index", mv.getViewName());		
	}
}
